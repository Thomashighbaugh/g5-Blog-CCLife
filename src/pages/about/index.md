---
templateKey: "about-page"
path: /about
title: About our values
---

## About Us

![A picture](/img/6.jpg)

This is another filler page, where our informaitonal description of the publication will go. I threw an image in here to make sure that the GraphiQL pile of dog ass actually is working (what a pain that crap is OMG).

**Note about Markdown**:
Markdown wraps the funcitonality of HTML in shortcuts, like a templating language such as Pug or Mustache. What that means is that you can use html directly in Markdown like so:

<figure>
    <img src="https://3.bp.blogspot.com/-zhmruBntWIE/T54t8dnG17I/AAAAAAAAG1k/NX1SrK5PNCM/s400/hulk-hogan-was-very-wrong-about-americas-love-for-pasta.jpg" alt="pastamania, brother">
</figure>
<figcaption>This is a figcaption, brother!</figcaption>

While this functionality is limited in its capacity and usefulness, it does offer a lot more to your <span style="color:#3333ff;">efforts writing markdown</span> that most people don't even know you can do (because most people don't think).

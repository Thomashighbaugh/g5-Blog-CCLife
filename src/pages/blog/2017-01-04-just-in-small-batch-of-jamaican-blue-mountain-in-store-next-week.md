---
templateKey: blog-post
title: Template Post
date: 2020-03-30T15:04:10.000Z
featuredpost: true
featuredimage: /img/1.jpg
description:
tags:
  - flavor
  - tasting
---

![Look Bombs!](/img/1.jpg)

# Template Page

This page is a template which makes for easy content creation locally, without needing to use the CMS system I worked
so hard on insuring was functional. I know, **I am awesome** but so is nuclear weaponry. So here is a few pictures.

![Boom!](/img/8.jpg)
